#ifndef ROTATIONIMAGE_WRITER_H
#define ROTATIONIMAGE_WRITER_H

#include "../image/header.h"
#include "../utils/state.h"

enum state to_bmp(FILE *out,  struct image const* image);
#endif //ROTATIONIMAGE_WRITER_H
