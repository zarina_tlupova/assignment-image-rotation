#include "utils/state.h"
#include "bmp_manager/reader.h"
#include "bmp_manager/writer.h"
#include "file_manager/file.h"
#include "image/header.h"
#include "rotator/rotator.h"
#include <stdio.h>


int main(int argc, char **argv) {

    if (argc != 3) {
        fprintf(stderr,"Need 2 arguments: First is the resource image, Second is a desired name for rotated image\n");
        exit(0);
    }

    const char *image = argv[1];
    const char *mod_image = argv[2];

    FILE *in = {0};
    enum state  state = file_open(image, &in, "r");
    if (state == OPEN_ERROR) {
        description(state);
        return 1;
    }
    struct image first_image = {0};
    state = from_bmp(in, &first_image);
    if (state != READ_OK) {
        description(state);
        file_close(in);
        return 1;
    }
    state = file_close(in);
    if(state == CLOSE_ERROR) {
        description(state);
        return 1;
    }
    struct image second_image = rotator(&first_image);
    delete_image(first_image);
    FILE *out = {0};
    state = file_open(mod_image, &out, "w");
    if (state == OPEN_ERROR) {
        description(state);
        file_close(out);
        return 1;
    }
    state = to_bmp(out, &second_image);
    if (state == WRITE_ERROR) {
        description(state);
        file_close(out);
        return 1;
    }
    state = file_close(out);
    if(state == CLOSE_ERROR) {
        description(state);
        return 1;
    }
    delete_image(second_image);
    description(READ_OK);

    return 0;
}

