#include "rotator.h"
#include "../image/header.h"
#include <malloc.h>

struct image rotator(const struct image *image) {



    struct pixel *data = malloc(sizeof(struct pixel) * image->width * image->height);


            for (size_t i = 0; i < image->height; ++i) {
                for (size_t j = 0; j < image->width; ++j) {
                    data[image->height * j + (image->height - 1 - i)] = image->data[i * image->width + j];
                }
            }

            return (struct image) {.width = image->height, .height = image->width, .data = data};

}
