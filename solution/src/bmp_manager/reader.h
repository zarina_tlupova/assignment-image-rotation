#ifndef ROTATIONIMAGE_READER_H
#define ROTATIONIMAGE_READER_H

#include "../image/header.h"
#include "../utils/state.h"


enum state from_bmp(FILE *in, struct image *image);

#endif //ROTATIONIMAGE_READER_H
