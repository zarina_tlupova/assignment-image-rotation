#ifndef ROTATIONIMAGE_STATE_H
#define ROTATIONIMAGE_STATE_H
enum state {
    READ_OK ,
    READ_INVALID_TYPE,
    OPEN_ERROR,
    CLOSE_ERROR,
    READ_INVALID_HEADER,
    WRITE_ERROR,
};

void description(const enum state state);

#endif //ROTATIONIMAGE_STATE_H
