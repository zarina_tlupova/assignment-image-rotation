#include "writer.h"
#include "../utils/state.h"

static uint32_t data_offset = 54;
static uint16_t format = 19778;
static uint32_t info_header_size = 40;
static uint16_t planes = 1;
static uint16_t bit_count = 24;

static size_t get_size_padded(const size_t width) {
    return width % 4 != 0?4 - ((width * sizeof(struct pixel)) % 4):0;
}

static uint32_t get_file_size(struct image const* image){
    return ((((image->width) * sizeof(struct pixel) + 3) / 4) * 4)*image->height + data_offset;
}
static uint32_t get_image_size(struct image const* image){
    return image->width*image->height*sizeof(struct pixel);
}

static struct bmp_header to_fill_header( struct image const* image) {

    return  (struct bmp_header) {
            .bfType = format,
            .bfileSize = get_file_size(image),
            .bfReserved = 0,
            .bOffBits = data_offset,
            .biSize = info_header_size,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = planes,
            .biBitCount = bit_count,
            .biCompression = 0,
            .biSizeImage = get_image_size(image) ,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}


enum state to_bmp(  FILE *out,  struct image const* image)
{
    struct bmp_header header =  to_fill_header(image);

    if(fwrite(&header, sizeof(struct bmp_header), 1, out)!=1) return WRITE_ERROR;

    if(fseek(out,  header.bOffBits, SEEK_SET)!=0) return WRITE_ERROR;
    const uint8_t zero = 0;

    if (image->data != NULL) {
        for (size_t i = 0; i < image->height; i++) {
            if(fwrite(image->data + i * image->width, image->width * sizeof(struct pixel), 1, out)!=1) return WRITE_ERROR;
            for (size_t j = 0; j < get_size_padded(image->width); j++) {
                if(fwrite(&zero, 1, 1, out)!=1) return WRITE_ERROR;
            }
        }
    } else {
        return WRITE_ERROR;
    }


    return READ_OK;
}

