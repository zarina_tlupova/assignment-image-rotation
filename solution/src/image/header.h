#ifndef ROTATIONIMAGE_HEADER_H
#define ROTATIONIMAGE_HEADER_H
#include "stddef.h"
#include "stdint.h"
#include <stdio.h>
#include <stdlib.h>

struct pixel {
    uint8_t b, g, r;
};
//!!!!!
struct image {
    size_t width, height;
    struct pixel *data;
};


struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
//static struct bmp_header writeHeader (const struct image *image);
#endif //ROTATIONIMAGE_HEADER_H
