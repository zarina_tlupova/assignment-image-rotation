#include "reader.h"


const uint16_t format = 0x4D42;

enum state from_bmp(FILE *in,  struct image *image) {

    struct bmp_header header = {0};
    if(fread(&header, sizeof(struct bmp_header), 1, in)!=1) return READ_INVALID_HEADER;

    if (header.bfType != format) {
        return READ_INVALID_TYPE;
    }

    const uint32_t bytes_per_pixel = ((uint32_t) header.biBitCount) / 8;

    image->width = header.biWidth;
    image->height = header.biHeight;
    image->data = malloc(image->width * image->height * sizeof(struct pixel));

    const uint32_t paddedRowSize = (((header.biWidth) * (bytes_per_pixel) + 3) / 4) * 4;

    for (size_t i = 0; i < image->height; i++) {

        if(fseek(in, (long) (header.bOffBits + (i * paddedRowSize)), SEEK_SET)!=0) return READ_INVALID_HEADER;

        for (size_t j = 0; j < image->width; j++) {
            if(fread(&(image->data[image->width * i + j]), sizeof(struct pixel), 1, in)!=1) return READ_INVALID_HEADER;
        }
    }

    return READ_OK;
}
