#include "state.h"
#include <stdio.h>

char *states_info[] = {

        [READ_OK] = "Program finished successfully ",
        [READ_INVALID_TYPE] = "Invalid type , ensure to use BMP format",
        [OPEN_ERROR] = "File could not be opened, please write the existing file in the argument.",
        [READ_INVALID_HEADER] = "Could not read out of file, invalid header",
        [CLOSE_ERROR] = "File could not be closed",
        [WRITE_ERROR] = "Could not write info to file"
};

void description( const enum state state) {
    fprintf(stdout,"Image state: %s\n", states_info[state]);
}
