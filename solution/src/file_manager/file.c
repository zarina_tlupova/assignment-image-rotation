#include "file.h"
#include <stdio.h>
#include <string.h>

enum state file_open(const char *file_name, FILE **file, char *mode) {
    enum state state = READ_OK;
    *file = fopen(file_name, mode);
    if (*file == NULL) {
        fprintf(stderr,"Error\n");
        state = OPEN_ERROR;
    }

    return state;
}

void delete_image(const struct image image) {
    free(image.data);
}

enum state file_close(FILE *file) {
    if (fclose(file) == EOF) {
        return CLOSE_ERROR;
    }
    return READ_OK;
}

