#ifndef ROTATIONIMAGE_FILE_H
#define ROTATIONIMAGE_FILE_H

#include "../image/header.h"
#include "../utils/state.h"
#include <bits/types/FILE.h>


enum state file_open(const char *file_name, FILE **file, char *mode);
enum state file_close(FILE *file);
void delete_image(const struct image image);



#endif //ROTATIONIMAGE_FILE_H
